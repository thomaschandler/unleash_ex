# Changelog

## 1.4.0
* Add Support for Capturing Metrics on Variants [!7](https://gitlab.com/afontaine/unleash_ex/merge_requests/7)

## 1.3.1
* Fix Missing Content-Type for Posting Metrics [89d073cf](https://gitlab.com/afontaine/unleash_ex/commit/89d073cf6e507816259c8481b9510c56db672deb)

## 1.3.0
* Fix Variant Serialization to JSON [e5f4fd3c](https://gitlab.com/afontaine/unleash_ex/commit/e5f4fd3cece12810afbe789c122404e9169bd1ef)
* Use Mojito to Pool HTTP Connections [e5f4fd3c](https://gitlab.com/afontaine/unleash_ex/commit/e5f4fd3cece12810afbe789c122404e9169bd1ef)

## 1.2.0
* Add Variant Support [!5](https://gitlab.com/afontaine/unleash_ex/merge_requests/5)
* Fix ETag Usage [f30b80bf](https://gitlab.com/afontaine/unleash_ex/commit/f30b80bf931f56f5de908ca738977c2e540155e4)

## 1.1.0
* Add Optional `Plug` Module [!6](https://gitlab.com/afontaine/unleash_ex/merge_requests/6)

## 1.0.0
* Implement Client Specification Tests [!4](https://gitlab.com/afontaine/unleash_ex/merge_requests/4)

## 0.2.0

* Add E-Tag Caching Support [!1](https://gitlab.com/afontaine/unleash_ex/merge_requests/1)
* Send the Library Version when Registering Client [!2](https://gitlab.com/afontaine/unleash_ex/merge_requests/2)
* Add Checks for a Retry Limit [!3](https://gitlab.com/afontaine/unleash_ex/merge_requests/3)

## 0.1.0

* Initial release

## Unreleased
